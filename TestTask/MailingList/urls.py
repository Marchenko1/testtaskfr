from django.urls import path

from .views import ClientAPIView, MailingAPIView

app_name = "MailingList"

urlpatterns = [
    # Главная страница

    path('api/v1/client_list/', ClientAPIView.as_view()),
    path('api/v1/client_list/<int:pk>/', ClientAPIView.as_view()),

    path('api/v1/mailing_list/', MailingAPIView.as_view()),
    path('api/v1/mailing_list/<int:pk>/', MailingAPIView.as_view()),

    # path('api/v1/message_list/', MessageAPIView.as_view()),
    # path('api/v1/message_list/<int:pk>/', MessageAPIView.as_view()),

    # # Запросы создания, обновления и удаления клиентов
    #
    # path('create_client/', client_create, name='client_create'),
    # path('update_client/<int:announcement_id>/', client_update, name="update_client"),
    # path('delete_client/<int:announcement_id>/', DeleteClientView.as_view(), name="delete_client"),
    #
    # # Запросы создания, обновления и удаления рассылок
    #
    # path('create_newsletter/', newsletter_create, name='create_newsletter'),
    # path('update_newsletter/<int:announcement_id>/', newsletter_update, name="update_newsletter"),
    # path('delete_newsletter/<int:announcement_id>/', DeleteNewsletterView.as_view(), name="delete_newsletter"),
    #
    # # Запросы получения статистики
    #
    # path('statistics/<int:statistics_id>/', statistics, name='statistics'),
    # path('statistics_all/', statistics_all, name='statistics_all'),
]
