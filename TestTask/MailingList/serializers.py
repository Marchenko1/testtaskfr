from abc import ABC

from rest_framework import serializers

from .models import Message, Client, Mailing


class ClientSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    phone_number = serializers.CharField(max_length=12)
    operator_code = serializers.CharField(max_length=10)
    tag = serializers.CharField(max_length=50)
    timezone = serializers.CharField(max_length=50)

    def create(self, validated_data):
        return Client.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.phone_number = validated_data.get('phone_number', instance.phone_number)
        instance.operator_code = validated_data.get('operator_code', instance.operator_code)
        instance.tag = validated_data.get('tag', instance.tag)
        instance.timezone = validated_data.get('timezone', instance.timezone)
        instance.save()
        return instance


class MailingSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    start_time = serializers.DateTimeField()
    message_text = serializers.CharField(max_length=255)
    client_filter_operator_code = serializers.CharField(max_length=10)
    client_filter_tag = serializers.CharField(max_length=10)
    end_time = serializers.DateTimeField()

    def create(self, validated_data):
        return Mailing.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.start_time = validated_data.get('start_time', instance.start_time)
        instance.message_text = validated_data.get('message_text', instance.message_text)
        instance.client_filter_operator_code = validated_data.get('client_filter_operator_code', instance.client_filter_operator_code)
        instance.client_filter_tag = validated_data.get('client_filter_tag', instance.client_filter_tag)
        instance.end_time = validated_data.get('start_time', instance.end_time)
        instance.save()
        return instance
