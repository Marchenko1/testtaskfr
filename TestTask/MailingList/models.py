from django.db import models


class Mailing(models.Model):
    """Сущность рассылки"""
    id = models.AutoField(primary_key=True)
    start_time = models.DateTimeField(help_text='Время старта рассылки')
    message_text = models.TextField(help_text='Текст сообщения')
    client_filter_operator_code = models.CharField(max_length=10, help_text='Фильтр кода оператора')
    client_filter_tag = models.CharField(max_length=50, help_text='Фильтр тэга клиентов')
    end_time = models.DateTimeField(help_text='Время окончания рассылки')

    def __str__(self):
        return f'{self.message_text}'


class Client(models.Model):
    """Сущность клиента"""
    id = models.AutoField(primary_key=True)
    phone_number = models.CharField(max_length=12, help_text='Номер телефона клиента')
    operator_code = models.CharField(max_length=10, help_text='Код оператора')
    tag = models.CharField(max_length=50, help_text='Тэг')
    timezone = models.CharField(max_length=50, help_text='Временная зона')

    def __str__(self):
        return f'{self.phone_number}'


class Message(models.Model):
    """Сущность сообщений"""
    id = models.AutoField(primary_key=True)
    creation_time = models.DateTimeField(null=False, auto_now=True, help_text='Дата создания сообщения')
    send_status = models.CharField(max_length=50, help_text='Статус отправки')
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.id}'
