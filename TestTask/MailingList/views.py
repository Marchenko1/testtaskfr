from django.forms import model_to_dict
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import Message, Client, Mailing
from django.shortcuts import render

from .serializers import ClientSerializer, MailingSerializer
from . import tasks

class ClientAPIView(APIView):
    def get(self, request):
        clients = Client.objects.all()
        return Response({'clients': ClientSerializer(clients, many=True).data})

    def post(self, request):
        serializer = ClientSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({'new_client': serializer.data})

    def put(self, request, *args, **kwargs):
        pk = kwargs.get('pk', None)
        if not pk:
            return Response({'error': 'Method PUT not allowed'})

        try:
            instance = Client.object.get(pk=pk)
        except ObjectDoesNotExist:
            return Response({'error': 'Object does not exists'})

        serializer = ClientSerializer(data=request.data, instance=instance)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({'client_update': serializer.data})

    def delete(self, request, *args, **kwargs):
        pk = kwargs.get('pk', None)
        if not pk:
            return Response({'error': 'Method DELETE not allowed'})

        try:
            Client.object.get(pk=pk).delete()
        except ObjectDoesNotExist:
            return Response({'error': 'Object does not exists'})

        return Response({'client_delete': 'True'})


class MailingAPIView(APIView):
    def get(self, request):
        mailing = Mailing.objects.all()
        tasks.task.delay()
        return Response({'mailing': MailingSerializer(mailing, many=True).data})

    def post(self, request):
        serializer = MailingSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({'new_mailing': serializer.data})

    def put(self, request, *args, **kwargs):
        pk = kwargs.get('pk', None)
        if not pk:
            return Response({'error': 'Method PUT not allowed'})

        try:
            instance = Mailing.object.get(pk=pk)
        except ObjectDoesNotExist:
            return Response({'error': 'Object does not exists'})

        serializer = MailingSerializer(data=request.data, instance=instance)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({'mailing_update': serializer.data})

    def delete(self, request, *args, **kwargs):
        pk = kwargs.get('pk', None)
        if not pk:
            return Response({'error': 'Method DELETE not allowed'})

        try:
            Mailing.object.get(pk=pk).delete()
        except ObjectDoesNotExist:
            return Response({'error': 'Object does not exists'})

        return Response({'mailing_delete': 'True'})
